$(document).ready(
    ()=>{

        $(".error").hide()
        $("#error").hide()

        const validateEmail = (email) => {
            return String(email)
                .toLowerCase()
                .match(
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            );
        };

        function checkEmail(element) {
            let res = false
            if(validateEmail($(element).val())){
                $(element).siblings("p").hide()
                res = true
            }else{
                $(element).siblings("p").show()
                res = false
            }
            
            $(element).next(".caption").css("top", "9px")
            return res

        }

        function checkPassword(element){
            let res = false
            if($(element).val().length == 0){
                res = false
                $(element).siblings("p").show()
            }else{
                res = true
                $(element).siblings("p").hide()
            }
            $(element).next(".caption").css("top", "9px")
            return res
        }
        
        $("body").on("blur", ".input-box #email",
            (e)=>{
                checkEmail(e.target)
            }
        )
            
        $("body").on("blur", ".input-box #password",
            (e)=>{
                checkPassword(e.target)
            }

        )

        $("body").on("click", "input[type='submit']",

            (e)=>{
                e.preventDefault();

                let pass = checkPassword(".input-box #password")
                let mail = checkEmail(".input-box #email")
                
                if( mail && pass){
                    $("#error").hide()
                }else{
                    $("#error").show()
                }
            }
        )
        
    }
)
