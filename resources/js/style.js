$(document).ready(() =>{
// ======================= main ====================
$(".menu").click(
    ()=>{
        let path = $(".menu img").attr('src')
        path = path == "./icons/menu.svg"?"./icons/close.svg":"./icons/menu.svg"
        $(".menu").toggleClass("active")
        $("#pages").toggleClass("active")
        let title = $(".menu").attr("title")
        title = title == "menu"?"close":"menu"
        $(".menu").attr("title", title)      
        setTimeout(()=>{$(".menu img").attr('src', path)}, 50)  
    }
);
    
let counter = 0;
$("#pages").children().each(
    function(i){
        $(this).css("--i", i)
        $("#pages").css("--length", counter++)
    }
)
        
        
$("header, main, footer").click(
    ()=>{
        $(".menu").attr("title", "menu")
        $(".menu").removeClass("active")
        $("#pages").removeClass("active")

        setTimeout(()=>{$(".menu img").attr('src', "./icons/menu.svg")}, 50)  

    }
)

$(window).scroll(
    ()=>{
        if(window.pageYOffset>100){
            $(".up").addClass('active')
        }else{
            $(".up").removeClass('active')
        }
    }
)  

$(".up").click(
    ()=>{
    scrollTo({top: 0, behavior: 'smooth'})
    }
)
            
// ======================= home ====================
$(".searchBar").keyup(
    (e)=>{
        if ($(e.target).val().length > 0){
            $("#x").removeClass("hidden")
        }else{
            $("#x").addClass("hidden")
        }
    }
)
    
$("#x").click(
    ()=>{
        $(".searchBar").val("")
        $("#x").addClass("hidden")
    }
)

// ======================= manga ====================

$("#chapters").children().each(
    function (index, elt){
        $("#chaps").append($(elt).firstChild)
    }
)

$("#chaps").children().removeClass("chapter")

$("#sort").click(
    ()=>{
        let path = $("#sort").attr("src")
        let order = $(".chapter").length

        if (path == "./icons/sort-down.png"){
            path = "./icons/sort-up.png"
            
        }else if (path == "./icons/sort-up.png") {
            path = "./icons/sort-down.png"
            order = 0
            
        }
        
        $("#sort").attr("src", path)
        $(".chapter").each(
            (i, elt)=>{
                if(order>0){
                    $(elt).css('order', order - i)
                }else{
                    $(elt).css('order', i)

                }
            }
        )
    }
)
// $(".read").hide()

$(".read").click(
    ()=>{
        console.log("sdfghj")
        let path = $("#read").attr("src")
        path = path === "./icons/read.svg" ? "./icons/not-read.svg" : "./icons/read.svg"
        $("#read").attr("src", path)
    }
)


// ======================= chapter ====================
        

len = $('.page').length
var page = 1


function movePage(index){
    $('.page').hide()

    if(index==1){
        $("#previous-page").hide()
    }else if(index==len){
        $("#next-page").hide()
    }else{
        $("#previous-page").show()
        $("#next-page").show()
    }
    $(`.page#${index}`).show()
    $("#page-control span").text(`page ${index}`)
}

$("#reading-style").click(
    (e)=>{
        var name = $(e.target).val()        
        if(name == "horizontal"){
            name = "vertical" 
            $("#page-control").removeClass("hidden")
            movePage(page)   

            $("body").removeClass("vertical")
            $("body").addClass("horizontal") 

        }else{
            name = "horizontal"
            $("#page-control").addClass("hidden")
            $(".page").show()

            $("body").removeClass("horizontal")
            $("body").addClass("vertical") 
        }
        
        $(e.target).val(name)   
    }
)

// computers

$('body').on('keydown', function(event) {
    if($("body").hasClass("horizontal")){
        if(event.keyCode == 37){
            page = page>1?page-1:1
            movePage(page)

        }else if (event.keyCode == 39){
            page = page<len?page+1:len
            movePage(page)
        }
    }
 });

// mobiles

$("body").on('touchstart', function(e){

    if($("body").hasClass("horizontal")){
        var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
        x = touch.pageX;
        
        if(x> window.innerWidth/2){
            page = page<len?page+1:len
            movePage(page)
        } else {
            page = page>1?page-1:1
            movePage(page)
        }
    }
})

// both

$("#previous-page").click(
    ()=>{
        page = page>1?page-1:1
        movePage(page)
    }
)
    
$("#next-page").click(
        ()=>{
        page = page<len?page+1:len
        movePage(page)
    }
)


// ======================= authentication ====================









// // Get degrees between each item, based on total items.
// var angleSteps = 360 / $('#pages-list li').length;
// // base angle to increment, which will rotate entire list.
// var baseAngle = 110;
// // center of the circle, relative to parent <ul>
// var center = 155;
// // distance to place each item from the center
// var distance = 140;

// updateListPositions()

// function updateListPositions()
// {
//     // loop through each list item and place it on the circle based on it's angle
//     $('#pages-list li').each(
//         function(index, element)
//         {
//             var angle = baseAngle + (index * angleSteps);
//             var x = distance * Math.cos(angle * (Math.PI / 180));
//             var y = distance * Math.sin(angle * (Math.PI / 180));
//             $(element).css({left:center+x, top:center+y});
//         });
//     }
    
//     var elt = document.getElementById("pages-list")
//     elt.addEventListener('wheel', move_wheel)
//     function move_wheel(event)
//     {
        
//         if (event.deltaY < 0 || event.deltaX < 0)
//         {
//             baseAngle = baseAngle - 5
//         }
//         else if (event.deltaY > 0 || event.deltaX > 0){
//             baseAngle = baseAngle + 5
//             window.scrollTo(0,0)
//     }
//     // console.log(baseAngle)
//     updateListPositions()
//     // window.scrollTo(0,0)
// };
// });

// // $("#pages-list").wheel(()=>{
// //     if(window.pageYOffset != 0){
// //         window.scrollTo(0,0)
// //     }
})

// // alert(window.innerHeight)

